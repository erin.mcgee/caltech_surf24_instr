# Generate IFO params 

import secrets
import os.path as pth
import random
import numpy as np

import finesse
import matplotlib.pyplot as plt


hash_file = pth.join(pth.dirname(__file__), '.hash')
llo_file = pth.join(pth.dirname(__file__), 'llo_O4.yaml')
print('Your hash file is at: ', hash_file)

try:
    with open(hash_file,'r') as f:
        hash_str = f.read().strip()
    
except FileNotFoundError:
    print('No hash file found, generating one now...')
    hash_str = None
else:
    print(f'Your uniquie hash is {hash_str}.')

if hash_str is None:
    hash_str = secrets.token_hex(nbytes=10)
    with open(hash_file,'w') as f:
        f.write(hash_str)
        print(f'Your uniquie hash is {hash_str} and has been written to disk for reuse.')

class GWD():
    def __init__(self, hash_str = hash_str):
        random.seed(a=hash_str)
        self.generation = random.randint(2, 3)
        powers = list([25, 50, 75, 100, 250, 500]) # Virgo -> ET-HF input powers
        
        if self.generation == 2:
            self.L = random.randrange(600, 4000, step=500) # ~ 1 km arms
            self.P = random.choice(powers[0:-3])
        else:
            self.L = 1e3*random.randrange(10, 40, step=5) # ~ 10 km arms
            self.P = random.choice(powers[3:])
    
    def print_params(self):
        print(f"Your detector is {self.L/1e3} km long with a maximum laser power of {self.P} W")
    
    def apply(self, model):
        model.Larm.value = self.L
        #model.LY = self.L
        model.L0.P.value = self.P

    def get_model(self):

        model = finesse.Model()
        model.parse(model_str)
        model.modes("off")  # planewave
        self.apply(model)
        
        return model

    def get_MI(self):
        model = self.get_model()
        
        # Turn "off" additional optics
        model.PRM.set_RTL(R=0,T=1)
        model.ITMX.set_RTL(R=0,T=1)
        model.ITMY.set_RTL(R=0,T=1)
        model.SRM.set_RTL(R=0,T=1)

        #model.ETMY.phi=45
        return model

    def get_FPMI(self):
        model = self.get_model()
        
        # Turn "off" additional optics
        model.PRM.set_RTL(R=0,T=1)
        model.SRM.set_RTL(R=0,T=1)

        #model.ETMY.phi = model.ITMY.phi = 45
        return model

    def get_PRFPMI(self):
        model = self.get_model()
        
        # Turn "off" additional optics
        model.SRM.set_RTL(R=0,T=1)

        return model

    def get_DRFPMI(self):
        model = self.get_model()
        
        return model


model_str = """
    ###########################################################################
    ###   Variables
    ###########################################################################
    var Larm 3995
    var Mtm  40
    var itmT 0.014
    var lmichx 4.5
    var lmichy 4.45

    ###########################################################################
    ###   Input optics
    ###########################################################################
    l L0 125
    s l_in L0.p1 PRM.p1
    # Power recycling mirror
    m PRM T=0.03 L=37.5u phi=90
    s prc PRM.p2 bs.p1 L=53


    # Central beamsplitter
    bs bs T=0.5 L=0 alpha=45

    ###########################################################################
    ###   X arm
    ###########################################################################
    s lx bs.p3 ITMX.p1 L=lmichx
    m ITMX T=itmT L=37.5u phi=90
    s LX ITMX.p2 ETMX.p1 L=Larm
    m ETMX T=5u L=37.5u phi=89.999875

    pendulum itmx_sus ITMX.mech mass=Mtm fz=1 Qz=1M
    pendulum etmx_sus ETMX.mech mass=Mtm fz=1 Qz=1M

    ###########################################################################
    ###   Y arm
    ###########################################################################
    s ly bs.p2 ITMY.p1 L=lmichy
    m ITMY T=itmT L=37.5u phi=0
    s LY ITMY.p2 ETMY.p1 L=Larm
    m ETMY T=5u L=37.5u phi=0.000125

    pendulum itmy_sus ITMY.mech mass=Mtm fz=1 Qz=1M
    pendulum etmy_sus ETMY.mech mass=Mtm fz=1 Qz=1M

    ###########################################################################
    ###   Output and squeezing
    ###########################################################################
    s src bs.p4 SRM.p1 L=50.525
    m SRM T=0.2 L=37.5u phi=-90
"""